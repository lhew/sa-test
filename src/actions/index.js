export const sendFilters = filter => ({type: 'SEND_FILTERS', filter});
export const removeFilter = index => ({type: 'REMOVE_FILTERS', index});
