import React, { Component } from "react";

import Map from "./components/Map/Map";
import Table from "./components/Table/Table";
import Filters from "./components/Filters/Filters";
import SelectedFilters from "./components/SelectedFilters/SelectedFilters";
import ConnectedChart from "./components/ConnectedChart/ConnectedChart";
import { uniq } from "./utils/";
import data from "./data.json";
import "./App.css";
import jsonQuery from "json-query";

window.d = data;
window.jq = jsonQuery;
window.u = uniq;

class App extends Component {
  render() {
    return (
      <main id="dashboard">
        <h1>Visualization assessment</h1>
        <Filters
          filterOptions={[
            {
              label: "Filter by name",
              key: "name"
            },
            {
              label: "Ownership type",
              key: "ownership_type",
              values: uniq(
                jsonQuery(["[*ownership_type]"], { data, allowRegexp: true })
                  .value
              )
            },
            {
              label: "Features",
              key: "features_products",
              values: uniq(
                jsonQuery(["[*features_products]"], { data, allowRegexp: true })
                  .value
              )
            }
          ]}
        />
        <SelectedFilters />
        <Map />
        <Table
          columns={[
            { key: "store_number", name: "Store" },
            { key: "name", name: "Name" },
            { key: "street_address", name: "Address" },
            { key: "phone_number", name: "Phone number" }
          ]}
        />
        <div className="frame charts">
          <div className="frame chart ownership">
            <ConnectedChart
              label="Coffeeshops by ownership type"
              filter="ownership_type"
            />
          </div>
          <div className="frame charts">
            <div className="frame chart features">
              <ConnectedChart
                label="Coffeeshops by features"
                filter="features_products"
              />
            </div>
          </div>
        </div>
      </main>
    );
  }
}

export default App;
