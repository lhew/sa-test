export const flattenFilters = filterObj => {
  if (filterObj) {
    return filterObj.reduce((obj, item) => {
      if (!obj[Object.keys(item)]) {
        obj[Object.keys(item)] = [Object.values(item)[0]];
      } else {
        obj[Object.keys(item)].push(Object.values(item)[0]);
      }

      return { ...obj };
    }, {});
  }
};


export const mountQuery = args => {
	return Object.keys(args).reduce((query, item) => {
		return query += `[*${item}~/(${args[item].join('|')})/i]`.toLowerCase();
}, '')
}

export const uniq = (arrArg) => {
  return arrArg.filter((elem, pos, arr) => {
    return arr.indexOf(elem) === pos;
  });
}