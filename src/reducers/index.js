import { combineReducers} from 'redux';

import coffeeShopsReducer from './coffeeShopsReducer'

export default combineReducers({
    coffeeShopsData: coffeeShopsReducer
});