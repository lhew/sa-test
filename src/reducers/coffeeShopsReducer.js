import data from "../data.json";
import jsonQuery from "json-query";
import { flattenFilters, mountQuery } from "../utils";
const initialState = {
  coffeeShops: data,
  filters: []
};

const coffeeShopsReducer = (state = initialState, action) => {
  let query;

  switch (action.type) {
    case "SEND_FILTERS":

      query = mountQuery(flattenFilters([...state.filters, action.filter]));
      return {
        coffeeShops: jsonQuery(query, { data, allowRegexp: true }).value,
        filters: [...state.filters, action.filter]
      };

    case "REMOVE_FILTERS":
      const updatedFilters = state.filters.filter(
        (filter, i) => i !== action.index
      );
      query = mountQuery(flattenFilters(updatedFilters)).value;

      return {
        coffeeShops: jsonQuery(query, { data, allowRegexp: true }).value,
        filters: updatedFilters
      };

    default:
      return state;
  }
};

export default coffeeShopsReducer;
