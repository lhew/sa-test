import React from "react";
import { connect } from "react-redux";
import { sendFilters} from "../../actions";

class Filters extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  onChangeItem(key, value) {

    this.setState({
      [key]: value
    });
  }

  sendFilters() {

    var st = {...this.state};
    Object.keys(st).forEach(key => {
      if(st[key]){
        this.props.sendFilters({[key]: st[key]})
        this.setState({[key]: null})
      }
    });
  }

  clearFilters() {
    this.setState(null);
  }

  render() {
    const { filterOptions} = this.props;

    return (
      <div className="frame filters">
        <form onSubmit={(e) => e.preventDefault()}>
        <h3>Filters</h3>
        <div className="filter-wrapper">
          {filterOptions.map(({ label, values, key }, j) => {
            return (
              <fieldset key={j} className="frame">
                <span>
                  Filter by <b>{label}</b>
                </span>{" "}
                <br />
                {values && (
                  <select
                    name={key}
                    onChange={event =>
                      this.onChangeItem(key, event.target.value)
                    }
                  >
                    {<option></option>}
                    {values.map((value, i) => (
                      <option key={i} value={value}>
                        {value}
                      </option>
                    ))}
                  </select>
                )}
                {!values && (
                  <input
                    type="text"
                    onChange={event =>
                      this.onChangeItem(key, event.target.value)
                    }
                  />
                )}
              </fieldset>
            );
          })}
          
        </div>
        <button type="reset" onClick={this.sendFilters.bind(this)}>Filter</button>&nbsp;&nbsp;
          {filterOptions && filterOptions.length > 0 && (
            <button type="reset" onClick={this.clearFilters.bind(this)}>Clean filters</button>
          )}
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({ filters: state.coffeeShopsData.filters });
const mapDispatchToProps = dispatch => ({
    sendFilters: filters => dispatch(sendFilters(filters)),
  })

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filters);
