import React from "react";
import { connect } from "react-redux";

const ReactBsTable  = require('react-bootstrap-table');
const BootstrapTable = ReactBsTable.BootstrapTable;
const TableHeaderColumn = ReactBsTable.TableHeaderColumn;

const DataTable = ({ columns, rows }) => {
  if (columns && Array.isArray(columns) && rows && Array.isArray(rows))
    return (
      <BootstrapTable data={rows}  striped hover pagination>
        {columns.map((column, i) => (
          <TableHeaderColumn isKey={i === 0} key={i} dataField={column.key}>
            {column.name}
          </TableHeaderColumn>
        ))}
      </BootstrapTable>
    );
  return <p className="no-data">No data to show</p>;
};

const mapStateToProps = state => ({ rows: state.coffeeShopsData.coffeeShops });

export default connect(
  mapStateToProps,
  null
)(DataTable);
