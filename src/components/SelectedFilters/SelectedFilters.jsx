import React from "react";
import { connect } from "react-redux";
import { removeFilter } from "../../actions";

class SelectedFilters extends React.Component {
  render() {
    const { coffeeShopsData: {filters}, removeFilter } = this.props;
    return (
      <div className="selectedFilters frame">
        <h4>Selected filters</h4>

        {filters &&
          filters.map((filter, index) => (
            <span
              className="filter-type"
              key={index}
              onClick={() => { removeFilter(index); }}
            >
              {Object.keys(filter)[0]} : {Object.values(filter)[0]}
              {" "} [x]
            </span>
          ))}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  coffeeShopsData: state.coffeeShopsData,
});

const mapDispatchToProps = dispatch => ({
  removeFilter: index => dispatch(removeFilter(index))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedFilters);
