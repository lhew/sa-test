import React from "react";
import { connect } from "react-redux";

const L = window.L;
L.Map.include({
  'clearLayers': function () {
    this.eachLayer(function (layer) {
      this.removeLayer(layer);
    }, this);
  }
});

class Map extends React.Component {
  constructor() {
    super();
    this.map = null;
    this.cluster = null;
  }

  renderMap(_data) {
    try {
      if(!this.map){
        this.map = L.map("mapContainer").setView([40.712, -74.227], 16);
      }

      if(this.cluster){
        this.map.removeLayer(this.cluster);
      }

      L.tileLayer(process.env.REACT_APP_TILE_LAYER, {
        attribution: process.env.REACT_APP_MAP_CONTRIBUTORS
      }).addTo(this.map);

      this.map.fitBounds([
        _data.coffeeShops.map(({ location: { latitude, longitude } }) => [
          latitude,
          longitude
        ])
      ]);

      this.cluster = L.markerClusterGroup({
        iconCreateFunction: function(cluster) {
          return L.divIcon({
            html: "<b>" + cluster.getChildCount() + "</b>",
            className: "mycluster",
            iconSize: L.point(40, 40)
          });
        },
        spiderfyOnMaxZoom: false,
        showCoverageOnHover: false,
        zoomToBoundsOnClick: false
      });

      _data.coffeeShops.forEach(item => {
        const {
          location: { latitude, longitude }
        } = item;

        this.cluster.addLayer(
          L.marker([latitude, longitude]).bindTooltip(`
            <strong>${item.name}</strong> <br />
            ${item.street_address} - ${item.zip} 
            <br />Phone: ${item.phone_number}`)
        );
      });
      this.map.addLayer(this.cluster);
    } catch (e) {
      console.log("opss ", e);
    }
  }

  componentDidUpdate() {
    this.renderMap(this.props);
  }

  componentDidMount() {
    this.renderMap(this.props);
  }

  render() {
    return <div className="frame map" id="mapContainer" />;
  }
}

const mapStateToProps = state => ({
  coffeeShops: state.coffeeShopsData.coffeeShops
});

export default connect(
  mapStateToProps,
  null
)(Map);
