import React from "react";
import { connect } from "react-redux";
import { Chart } from "react-charts";
import jsonQuery from "json-query";
import { uniq } from "../../utils";

const ConnectedChart = ({ columns, rows, label, samples, options }) => {

  return (
    <React.Fragment>
      <h5>{label}</h5>
      <Chart
        data={[
          {
              data: samples.map((sample, i) => {
                
                return [i, sample.length, options[i]] 
              })
          },
        ]}
        series={{ type: "bar" }}
        axes={[
          { primary: true, type: "ordinal", position: "bottom" },
          { position: "left", type: "linear", stacked: true }
        ]}
        primaryCursor
        secondaryCursor
        tooltip
      />
    </React.Fragment>
  );
};
const mapStateToProps = (state, props) => {


  const options = uniq(
    jsonQuery([`[*${props.filter}]`], {
      data: state.coffeeShopsData.coffeeShops,
      allowRegexp: true
    }).value
  );
  
  return {
    options,
    samples: options.map(option => {
      return jsonQuery([`[*${props.filter}=${option}]`], {
        data: state.coffeeShopsData.coffeeShops,
        allowRegexp: true
      }).value;
    })
  };
};
export default connect(
  mapStateToProps,
  null
)(ConnectedChart);
