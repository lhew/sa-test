This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

##Assessment goals:

Assignment: 

 

Using dataset of Starbucks Locations (https://opendata.socrata.com/Business/USA-Starbucks/e3xz-8cw7) implement web dashboard with following features:
1) Map component showing locations of Starbucks (use Leaflet with MarkerCluster plugin to cluster points for performance; ideally: add switch to select between showing just as dots or also as clusters). Points should have popups with information about coffeeshop.
2) Table with paginated entries
2) Bar Chart showing distribution of coffeshops by ownership (‘Ownership Type’ column)
3) Bar Chart showing distribution of coffeshop features (how many coffeshops provide this feature, using ‘Features-Products’ column)
4) Add search/filtering capability by ‘Name’ (input field)
5) Add filtering capability by ownership type
6) Add filtering capability by products features
7) Create reusable component for applied filters listing and removal with ‘Reset All’ button to remove all applied filters and buttons for individual filters removal.

Charts, Table and Map should reflect changes made to dataset by filters. 

Technology: use Vue/React with any visual style framework, charting library of your choice. Use Vuex/Redux or similar for state management (might be an overkill but good to see).

 ## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
